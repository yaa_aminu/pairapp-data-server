'use strict'
var port = process.env.PORT || 4000
var debug = require('debug')('pairapp:config'),
    redisUrl = '',
    mongodb_url = '';
(function makeRedisUrl() {
    debug('current environment: %s', process.env.NODE_ENV || 'production')
    if (process.env.NODE_ENV == 'dev') {
        //do nothng let the client use defaults
        redisUrl = {
            url: 'localhost',
            PORT: 6379
        }
        mongodb_url = 'mongodb://localhost/chat-server-data'

    } else if (process.env.NODE_ENV === 'test') {
        redisUrl = {
            url: 'localhost',
            PORT: 6379
        }
        mongodb_url = 'mongodb://localhost/pairapp-test'
    } else {
        redisUrl = {
            url: 'localhost',
            PORT: 6379
        }
        //TODO use a production mongodb server
        // mongodb_url = 'mongodb://yennkasa:PJbopywyFBZtNN3cVrVZdYmzSZUdT2ygILV5rp7BSYMkrQsPxfscW0CVKC98zl40@45.32.154.171/yennkasa'
        mongodb_url = 'mongodb://pairapp-chat:nbgWRvmppFqYYVZxneIOOHPZDZ2LaxmxI1QXn@ds143737.mlab.com:43737/pairapp-chat-server-log'
    }
})();

exports = module.exports = {
    jwt_secrete: 'AI0+@<kaqx8a7fa&6!@za($(#8373472$#@!34d-a-af=qSyDazc)%$d6)(*&65xts549ijlbncAtJcEKw4',
    jwt_issuer: 'pairapp-server',
    jwt_expires_in: 60 * 60 * 24, //1 day
    port: port,
    serverURL: process.env.NODE_ENV === 'dev' ? 'http://localhost:' + port : 'https://api.yennkasa.com/parse/',
    authToken: process.env.NODE_ENV === 'dev' ? '' : 't4TOKyq4LqgUJPQcTjkVwQfJYsK6YoGSLlnbgWRvmppFqYYVZxneIOOHPZDZ2LaxmxI1QXnKEqyHQvc4ZpEkiw==',
    mongo_uri: mongodb_url,
    redis_url: redisUrl.url,
    redis_port: redisUrl.PORT,
    redis_auth: process.env.REDIS_PASS,
    twilioSID: 'ACefd4809459687575f8f0ba3b9e6f43b2',
    twilioAuth: '13ee787c5ac62bb8ce1690f7e4c77965',
    twilioNumber: '+18188774826',
    sinch_api_key: "465fcda7-d6df-4b6c-981c-ff4d06879653",
    sinch_secrete: "gk8ru6rT5EaE6l6yt0bUbw==",
    file_api_auth: "3kJOVlPJFLJddlEsUTBHiKCXkVrbswWI5BZxLG7qR1A2fPuERP0n6OR62xb1saSuYXnFmb6IygAJhIvvaq42YCjhwKHKGwzyjxVVqErs0a3pRxZ4zOz9BkwN1jf0iRJG"
}
