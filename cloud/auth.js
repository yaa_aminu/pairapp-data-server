'use strict'
var jwt = require('jsonwebtoken')

//FIXME implement authorization
exports.authorize = function(authToken, cb) {
  jwt.verify(authToken, config.jwt_secrete, {
    algorithm: 'HS256',
    'issuer': config.jwt_issuer
  }, cb)
}

exports.authorizePushRegistrationRequest = authToken => authToken === config.authToken

exports.signJwt = (payload, cb) => {
  jwt.sign(payload, config.jwt_secrete, {
    algorithm: 'HS256',
    issuer: config.jwt_issuer,
    expiresIn: config.jwt_expires_in
  }, cb)
}


exports.signJwtSync = payload => jwt.sign(payload, config.jwt_secrete, {
  algorithm: 'HS256',
  issuer: config.jwt_issuer
})
