'use strict'
const debug = require('debug')('pairapp:cloudCode'),
    auth = require('./auth'),
    crypto = require('crypto'),
    twilioClient = require('twilio')(config.twilioSID, config.twilioAuth)

Parse.serverURL = config.serverURL

function makeParseQuery(className) {
    var obj = Parse.Object.extend(className)
    return new Parse.Query(obj)
}

function newParseObject(className) {
    if (!className) throw new Error('className must be provided')
    var o = Parse.Object.extend(className)
    return new o()
}

function PCallbackWrapper(sessionToken, cb) {
    cb = cb || sessionToken
    let opts = {
        useMasterKey: true,
        success: function(parseObjects) {
            //some parse queries like query#first() calls the success callback with undefined
            //when there is no match found
            if (!parseObjects) return cb({
                message: 'Object Not Found',
                code: Parse.Error.OBJECT_NOT_FOUND
            })
            cb(null, parseObjects)
        },
        error: function(obj, error) {
            //some parse operations like save passes two params to the error callback
            //when only one params is passed error will be undefined so obj will denote the error
            error = error || obj
            cb(error)
        }
    }
    if (typeof sessionToken == 'string') {
        opts.sessionToken = sessionToken
    }
    debug(opts)
    return opts
}


Parse.Cloud.beforeSave('_User', (request, response) => {
    debug('beforeSave User')
    let userId = request.object.get('userId')
    if (userId.indexOf('-') !== -1 || userId.indexOf('_') !== -1) {
        return response.error('userId must not contain \'-\' or \'_\'')
    }
    return response.success()
})

Parse.Cloud.beforeSave('group', (request, response) => {
    debug('beforeSave group')
    let userId = request.object.get('userId')
    debug('about to verify %s', userId)
    if (userId.indexOf('-') !== -1 || userId.indexOf('_') !== -1 || userId.indexOf('@') !== userId.lastIndexOf('@')) {
        debug('invalid group name')
        return response.error('group names must not contain \'-\',\'_\' or more than one \'@\'')
    }
    response.success()
})

Parse.Cloud.define('createGroup', (request, response) => {
    debug('cloud function createGroup()')
    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'you are unauthorised'
    })

    if (!request.params.name || !request.params.members) {
        return response.error({
            code: Parse.Error.OPERATION_FORBIDDEN,
            message: 'name and members required'
        })
    }

    if (!Array.isArray(request.params.members)) {
        request.params.members = request.params.members.split(',')
    }
    if (request.params.members.length < 2) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'cannot create a group with less than 2 members'
    })

    debug('name: %s, members: %s', request.params.name, request.params.members)
    let userId = request.params.name + '@' + request.user.get('userId')
    makeParseQuery('group')
        .equalTo('userId', userId)
        .first(PCallbackWrapper((err, item) => {
            debug(err || item)
            if (item) return response.error({
                code: Parse.Error.DUPLICATE_VALUE,
                message: 'group already exists'
            })
            if (err && err.code !== Parse.Error.OBJECT_NOT_FOUND) {
                return response.error(err)
            }
            let group = newParseObject('group')
            group.set('name', request.params.name)
            group.set('userId', userId)
            request.params.members.forEach(member => group.addUnique('members', member))
            group.set('DP', request.params.DP || 'avatar_empty')
            group.set('admin', request.user)
            let adminJson = request.user.toJSON()
            delete adminJson.ACL
            group.set('adminJson', adminJson)
            group.set('createdBy', request.user.get('userId'))
            userStore.addPushIdGroup(userId, request.params.members, err => {
                debug(err)
                if (err) return response.error(err)
                group.save(PCallbackWrapper((err, group) => {
                    debug(group, err)
                    if (err) return userStore.removePushIdGroup(userId, err2 => {
                        debug(err2 || userId + " successfully removed")
                        return response.error(err)
                    })
                    return response.success(group)
                }))
            })
        }))
})

Parse.Cloud.define('addMembers', (request, response) => {
    debug('cloud function "addMembers()"')
    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'you are unauthorised'
    })
    let sessionToken = request.user.getSessionToken()

    if (!request.params.members || !request.params.userId) return response.error({
        code: Parse.Error.INVALID_JSON,
        message: 'some required fields were no filled'
    })

    if (!Array.isArray(request.params.members)) {
        request.params.members = request.params.members.split(',')
    }
    makeParseQuery('group')
        .equalTo('userId', request.params.userId)
        .first(PCallbackWrapper(sessionToken, (err, item) => {
            debug(err, item)
            if (err) return response.error(err)

            //TODO yaaminu: should we allow non-admins to add new members?
            request.params.members.forEach(member => item.addUnique('members', member))

            userStore.addPushIdGroup(request.params.userId, request.params.members, err => {
                debug(err)
                if (err) return response.error(err)
                item.save(PCallbackWrapper(err => {
                    debug(err)
                    if (err) {
                        return userStore
                            .removePushIdGroup(request.params.userId,
                                request.params.members, err2 => {
                                    debug(err2)
                                    return response.error(err)
                                })
                    }
                    return response.success()
                }))
            })
        }))
})

Parse.Cloud.define('removeMembers', (request, response) => {
    debug('cloud function "removeMembers()"')
    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'you are unauthorised'
    })
    let sessionToken = request.user.getSessionToken()

    if (!request.params.members || !request.params.userId) return response.error({
        code: Parse.Error.INVALID_JSON,
        message: 'some required fields were no filled'
    })

    if (!Array.isArray(request.params.members)) {
        request.params.members = request.params.members.split(',')
    }
    if (request.params.members.length == 0) {
        return response.Error(parse.Error.BAD_JSON, {
            code: Parse.Error.BAD_JSON,
            message: 'No member specified'
        })
    }
    makeParseQuery('group')
        .equalTo('userId', request.params.userId)
        .first(PCallbackWrapper(sessionToken, (err, item) => {
            debug(err, item)
            if (err) return response.error(err)
            if (item.get('createdBy') === request.user.get('userId') &&
                request.params.members.indexOf(request.user.get('userId')) !== -1) {

                return response.error(Parse.Error.OPERATION_FORBIDDEN, {
                    code: Parse.Error.OPERATION_FORBIDDEN,
                    message: 'You are not allowed to remove yourself from your own group'
                })
            }

            if (item.get('createdBy') !== request.user.get('userId') &&
                (request.params.members[0] !== request.user.get('userId'))) {
                return response.error(Parse.Error.OPERATION_FORBIDDEN, {
                    code: Parse.Error.OPERATION_FORBIDDEN,
                    message: 'You are not allowed to remove other members from this group'
                })
            }

            item.removeAll('members', request.params.members)
            userStore.removePushIdGroup(request.params.userId, request.params.members, err => {
                debug(err)
                if (err) return response.error(err)
                item.save(PCallbackWrapper(err => {
                    debug(err)
                    if (err) {
                        return response.error(err)
                    }
                    return response.success()
                }))
            })
        }))

})

Parse.Cloud.define('genSinchToken', (request, response) => {
    debug('cloud function "genSinchToken()"')
    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'you are unauthorised'
    })

    let userId = request.user.get('userId')
    userStore.nextSinchSequence((err, sequence) => {
        if (err) return response.error(err)
        return response.success({
            token: finallyGenerateSinchToken(userId, sequence),
            sequence: sequence
        })
    })
})

let finallyGenerateSinchToken = (userId, sequence) => {
    let toSign = userId + config.sinch_api_key + sequence + config.sinch_secrete
    let hash = crypto.createHash('sha1');
    hash.update(toSign);
    hash = hash.digest('base64').trim()
    debug(hash)
    return hash
}


Parse.Cloud.define('genToken', (request, response) => {
    debug('cloud function "genToken()"')
    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'you are unauthorised'
    })
    if (request.params.newPushId && !request.params.pushId) return response.error({
        code: Parse.Error.INVALID_JSON,
        message: 'some required fields were no filled'
    })
    debug(request.user.get('userId'))
    
    if (request.params.newPushId) {
        debug(request.params.pushId)
    }

    if (request.params.newPushId) {
        userStore.addPushId(request.user.get('userId'), request.params.pushId, err => {
            if (err) return response.error(err)
            return genToken(request.user.get('userId'), (err, token) => {
                debug(err, token)
                if (err) return response.error({
                    code: Parse.Error.INTERNAL_SERVER_ERROR,
                    message: 'Error while generating auth token'
                })
                response.success(token)
            })
        })
    } else {
        return genToken(request.user.get('userId'), (err, token) => {
            debug(err, token)
            if (err) return response.error({
                code: Parse.Error.INTERNAL_SERVER_ERROR,
                message: 'Error while generating auth token'
            })
            response.success(token)
        })
    }
})

function genToken(userId, cb) {
    return auth.signJwt({
        userId: userId
    }, cb)
}

Parse.Cloud.beforeSave('_Installation', (request, response) => {
    debug('before save installation')
    let pushId = request.object.get('pushId')
    if (!pushId)
        return response.error({
            code: Parse.Error.BAD_JSON,
            message: 'can\'t save an installation without push id'
        })
    let userId = request.object.get('userId')
    if (!userId)
        return response.error({
            code: Parse.Error.BAD_JSON,
            message: 'can\'t save an installation without user id'
        })

    switch (request.object.get('deviceType')) {
        case 'android':
            return userStore.addPushId(userId, pushId, (err) => {
                debug(err || 'successfully registered device')
                if (err) return response.error(err)
                return response.success()
            })
        default:
            return response.error({
                code: Parse.Error.OPERATION_FORBIDDEN,
                message: 'Only android devices are supported'
            })
    }
})

Parse.Cloud.beforeDelete('group', (request, response) => {
    debug('cloud function beforeDelete')
    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'you are unauthorised'
    })

    userStore.removePushIdGroup(request.object.get('userId'), err => {
        debug(err || 'removed all pushids')
        if (err) return response.error(err)
        response.success()
    })
})

Parse.Cloud.define('setPublicKeyForUser', (request, response) => {
    debug('cloud function setPublicKeyForUser')
    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'unauthorised'
    })
    if (!request.params.publicKey) return response.error({
        code: Parse.Error.BAD_JSON,
        message: 'required fields not filled'
    })
    makeParseQuery('publicKey')
        .equalTo('userId', request.user.get('userId'))
        .select('user_public_key_rsa')
        .first(PCallbackWrapper((err, object) => {
		debug("arguments")
			debug(arguments)
            if (err) {
                if (err.code !== Parse.Error.OBJECT_NOT_FOUND) {
                    return response.error(err)
                }
            }
            object = object || newParseObject('publicKey')
            object.set('user_public_key_rsa', request.params.publicKey)
            object.set('userId', request.user.get('userId'))
            object.save(PCallbackWrapper((err) => {
		debug(typeof err)
                if (err) return response.error(err)
                return response.success()
            }))

        }))
})


Parse.Cloud.define('getPublicKeyForUser', (request, response) => {
    debug('cloud function getPublicKeyForUser')
    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'you are unauthorised'
    })

    if (!request.params.userId) {
        return response.error({
            code: Parse.Error.BAD_JSON,
            message: 'required fields not filled'
        })
    }
    makeParseQuery('publicKey')
        .equalTo('userId', request.params.userId)
        .select('user_public_key_rsa')
        .first(PCallbackWrapper((err, object) => {
            debug('error while setting public key')
            if (err) return response.error(err)
            return response.success(object.get('user_public_key_rsa'))
        }))

})


Parse.Cloud.define('search', function(request, response) {
    return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'search not available'
    })
    let params = request.params.query,
        query = {}
    if (typeof params === 'string') {
        query.name = params
        query.city = params
    } else {
        query.city = params.city || ''
        query.name = params.name || ''
    }
    if (!query.city && !query.name) {
        return response.error({
            code: Parse.Error.BAD_JSON,
            message: 'empty query'
        })
    }
    if (!query.name.startsWith('@')) {
        query.name = '@' + query.name
    }
    debug(query)
    let nameQuery = makeParseQuery('_User')
        .startsWith('name', query.name)
    let cityQuery = makeParseQuery('_User')
        .startsWith('city', query.city)

    Parse.Query.or(nameQuery, cityQuery)
        .find(PCallbackWrapper((err, users) => {
            debug(err || users)
            if (err) return response.error(err)
            return response.success(users)
        }))
})

Parse.Cloud.define('sendVerificationToken', (request, response) => {

    if (!request.user) return response.error({
        code: Parse.Error.OPERATION_FORBIDDEN,
        message: 'you are unauthorised'
    })

    if (request.user.get('verified') === true) {
        return response.success()
    }

    if (typeof request.params.message != 'string') return response.error({
        code: Parse.Error.BAD_JSON,
        message: 'required field missing'
    })

    twilioClient.sendMessage({
        to: '+' + request.user.get('userId'),
        from: config.twilioNumber,
        body: request.params.message
    }, (err, responseData) => {
        debug(err || responseData)
        if (err) return response.error(err)
        return response.success()
    })
})
