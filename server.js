'use strict'

global.config = require('./config')
global.userStore = require('./userstore')
const express = require('express')
const debug = require('debug')('pairapp:server')
const rateLimiter = require('express-rate-limit')
const fs = require('fs')

const ParseServer = require('parse-server').ParseServer
var api = new ParseServer({
    databaseURI: config.mongo_uri,
    cloud: require('path').resolve(__dirname, 'cloud/main.js'),
    appId: 'RcCxnXwO1mpkSNrU9u4zMtxQac4uabLNIFa662ZY',
    masterKey: 'MtKTTHhNnpafPSOpBxsovZpXPpewl+BV9jRc+0MspdYRtD3FYXSzaRx0VNFmSIfBYR1zBD5' +
        'aQtFy+vdYo5IAA5MdPVsuMjwCqhwlgeQKDjY1aFHg9132NUJGi9vzVpiFFzMyzSo3U5qNN4YydYBzArA0uHtDi' +
        'nGIaVFX8C0=', //TODO amin change the aster key
    serverURL: config.serverURL,
    allowClientClassCreation: false
})

var app = express()

app.enable('trust proxy');

app.use(express.static('static'))

var limiter = new rateLimiter({
    windowMs: 5 * 60 * 1000,
    max: 1,
    delayMs: 0
});

app.use(/sendVerificationToken/i, limiter)
app.get('/', function(req, res) {
    res.set('content-type', 'text/html')
    fs.createReadStream('./index.html').pipe(res)
})
app.get('/dl', (req, res) => {
    res.redirect('/yennkasa.apk')
})

if (process.env.NODE_ENV == 'dev') {
    app.use(require('morgan')('dev'))
}

function authorize(req, res, next) {
    if (req.method === 'GET') {
        return next()
    }
    if (req.get('X-Authorization-File') === config.file_api_auth) {
        return next()
    }
    res.status(401).json({
        message: 'unauthorized'
    })
}

app.use('/parse/files/', authorize)

app.use('/files/', authorize)

app.use('/parse', api)
app.use('/', api)

userStore.connect(() => {})
app.listen(config.port, function() {
    debug('server running on port ' + config.port + '.')
})