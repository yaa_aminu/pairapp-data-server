'use strict'
const redis = require('redis'),
    debug = require('debug')('pairapp:userstore')


var client

const onRedisClientError = err => debug('error on redis client %s', err)

const onRedisClientConnected = () => debug('connected to redis @ %s', new Date())


function connect() {
    client = redis.createClient(config.redis_port, config.redis_url)
    client.on('connect', onRedisClientConnected)
    client.on('error', onRedisClientError)
    client.auth(config.redis_auth)
}

var nextSinchSequence = cb => {
    client.incr('sinc.nonce.sequence', cb)
}

var getPushIds = function(userId, cb) {
    if (!userId) {
        return cb(new Error('invalid userId and/or pushIds'))
    }

    client.get(userId.trim(), (err, reply) => {
        debug(err || reply)
        if (err) return cb(err)
        cb(err, reply)
    })
}

var getPushIdsGroups = function(userId, cb) {
    if (!userId) return cb(new Error('invalid userId'))
    client.smembers(userId, (err, members) => {
        let batch = client.batch()
        members.forEach(member => batch.get(member))
        batch.exec((err, items) => {
            if (err) return cb(err)
            items = items.filter(item => item && !(item instanceof Error))
            cb(null, items)
        })
    })
}

var addPushId = function(userId, pushId, cb) {
    if (!userId || !pushId) {
        return cb(new Error('invalid userId and/or pushIds'))
    }
    client.set(userId, pushId, cb)
}

var addPushIdGroup = function(userId, memberIds, cb) {
    if (!userId || !memberIds) {
        return cb(new Error('invalid userId and/or memberId'))
    }
    if (!Array.isArray(memberIds)) {
        memberIds = [memberIds]
    }
    return client.sadd(userId, memberIds, cb)
}

//TODO Amin, make sure oldPushId is actually
//the current value before removing
var removePushId = function(userId, oldPushId, cb) {
    if (!userId || !oldPushId) {
        return cb(new Error('invalid userId and/or pushIds'))
    }
    client.get(userId, (err, reply) => {
        if (err) return cb(err)
        if (reply !== oldPushId) return cb()
        return client.del(userId.trim(), cb)
    })
}

var removePushIdGroup = (userId, memberId, cb) => {
    if (!userId) {
        return cb(new Error('invalid userId'))
    }
    if (!cb) {
        cb = memberId
    }
    if (!cb) throw new Error('at least 2 args required')

    if (typeof memberId === 'function') {
        return client.del(userId, cb)
    }

    client.srem(userId, memberId, cb)
}

exports.connect = connect
exports.removePushIdGroup = removePushIdGroup
exports.removePushId = removePushId
exports.addPushId = addPushId
exports.addPushIdGroup = addPushIdGroup
exports.getPushIds = getPushIds
exports.getPushIdsGroups = getPushIdsGroups
exports.nextSinchSequence = nextSinchSequence
